from dsr.doc_events.delivery_note import discount_set_in_taxes, set_payment_percent, update_discounts, update_payment_method_total, validate_payment_method_total
from dsr.utils.taxes_and_totals import dsr_calculate_taxes_and_totals
from erpnext.accounts.doctype.sales_invoice.sales_invoice import SalesInvoice
from types import MethodType


def sales_invoice_events(doc, method=None):
    if doc.naming_series != 'SV-.YYYY.-':
        return
    # Set Items property
    if method in ('onload', 'before_validate', 'before_cancel', 'before_update_after_submit', 'before_submit'):
        doc.items = doc.get('pos_items', default=[]) + doc.get('non_pos_items', default=[])
        doc.load_from_db = MethodType(load_from_db, doc)
        # TODO: Remove after parent app update event code by cheking fields.
        if not hasattr(doc, "taxes"):
            doc.taxes = []
        if not hasattr(doc, 'discount_table_items'):
            doc.discount_table_items = []
        if doc.total_discounts is None:
            doc.total_discounts = 0

    # Calculate.
    if method in ('before_save', 'before_submit'):
        update_payment_method_total(doc)
        update_discounts(doc)
        discount_set_in_taxes(doc)
        dsr_calculate_taxes_and_totals(doc)
        set_payment_percent(doc)
        if method == 'before_submit':
            validate_payment_method_total(doc)


def load_from_db(self):
    super(SalesInvoice, self).load_from_db()
    self.items = self.get('pos_items', default=[]) + self.get('non_pos_items', default=[])
