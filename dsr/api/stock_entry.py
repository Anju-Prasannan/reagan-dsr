from restaurant_customizations.restaurant_customizations.material_request import make_stock_entry
import frappe

@frappe.whitelist(methods="POST")
def make_entry_from_material_request(source_name, target_doc=None):
    doc = make_stock_entry(source_name=source_name, target_doc=target_doc)
    if doc.stock_entry_type == "Store to Store Transfer":
        doc.stock_entry_type = "Material Transfer"
        doc.purpose = "Material Transfer"
    return doc
