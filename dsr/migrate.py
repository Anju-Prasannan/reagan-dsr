import frappe


def after_migrate():
    remove_dsr_un_used_field()


def remove_dsr_un_used_field():
    # Remove Custom Field.
    custom_fields = ["Delivery Note-create_type", "Delivery Note Item-create_type",
                     "Delivery Note-discount_table_items",
                     "Sales Invoice-create_type", "Sales Invoice-discount_table_items"]
    for cf in custom_fields:
        frappe.delete_doc_if_exists("Custom Field", cf)
