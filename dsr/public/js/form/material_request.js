// Zone Filter
{% include "dsr/public/js/form/location_filter.js" %}


frappe.ui.form.on("Material Request", {
    make_stock_entry: frm => {
        frappe.model.open_mapped_doc({
            method: "dsr.api.stock_entry.make_entry_from_material_request",
            frm: frm
        })
    }
})