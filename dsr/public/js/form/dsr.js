const doctype_events = {
    refresh: frm => {
        frm.events.toggle_reqd_items(frm)
        frm.events.remove_discount_amount_event(frm)
        frm.events.make_update_custom_button(frm)
    },
    make_update_custom_button: frm => {
        frm.add_custom_button(__("Update"), ()=>{
            frappe.xcall("dsr.doc_events.delivery_note.update_delivery", {
                doc: frm.doc
            }).then(r=>{
                if(r) {
                    frappe.model.sync(r)
                    frm.refresh_fields()
                }
            })
        })
    },
    naming_series: frm => {
        frm.events.toggle_reqd_items(frm)
        frm.events.remove_discount_amount_event(frm)
    },
    remove_discount_amount_event: frm => {
        // For Disable, Discount Default.
        if (["DSR-.YYY.-", "DSR-.YYYY.-", "SV-.YYYY.-"].includes(frm.doc.naming_series) && frm.cscript.discount_amount) {
            delete frm.cscript.discount_amount
        }
    },
    toggle_reqd_items: frm =>{
        frm.toggle_reqd("pos_items", ["DSR-.YYY.-", "DSR-.YYYY.-", "SV-.YYYY.-"].includes(frm.doc.naming_series) && !frm.doc.non_pos_items)
        frm.toggle_reqd("non_pos_items", ["DSR-.YYY.-", "DSR-.YYYY.-", "SV-.YYYY.-"].includes(frm.doc.naming_series) && !frm.doc.pos_items)
        frm.toggle_reqd("items", !["DSR-.YYY.-", "DSR-.YYYY.-", "SV-.YYYY.-"].includes(frm.doc.naming_series))
    },
    set_warehouse: frm => {
		frm.cscript.autofill_warehouse(frm.doc.non_pos_items, "warehouse", frm.doc.set_warehouse);
		frm.cscript.autofill_warehouse(frm.doc.pos_items, "warehouse", frm.doc.set_warehouse);
	},
    to_warehouse: frm => {
		frm.cscript.autofill_warehouse(frm.doc.non_pos_items, "warehouse", frm.doc.to_warehouse);
		frm.cscript.autofill_warehouse(frm.doc.pos_items, "warehouse", frm.doc.to_warehouse);
	},
    customer: frm => {
        if (frm.doc.customer) {
            frappe.xcall("dsr.doc_events.delivery_note.get_customer_warehouse", {
                customer: frm.doc.customer 
            }).then(r=> {
                if (r) {
                    frm.set_value('set_warehouse', r)
                }
            })
        }
    }
}


const item_doctype_events = {
    pos_items_add: frm => {
        frm.events.toggle_reqd_items(frm)
    },
    non_pos_items_add: frm => {
        frm.events.toggle_reqd_items(frm)
    },
    pos_items_remove: frm => {
        frm.events.toggle_reqd_items(frm)
    },
    non_pos_items_remove: frm => {
        frm.events.toggle_reqd_items(frm)
    }
}



const calculate_payment_method_total = (cdt, cdn) =>{
    let row = frappe.get_doc(cdt, cdn)
    frappe.model.set_value(cdt, cdn, 'amount', flt(row.pos_amount) + flt(row.non_pos_amount))
}

frappe.ui.form.on("Payment Method Item", {
    pos_amount: (frm, cdt, cdn)=>{
        calculate_payment_method_total(cdt, cdn)
    },
    non_pos_amount: (frm, cdt, cdn)=>{
        calculate_payment_method_total(cdt, cdn)
    }
})
