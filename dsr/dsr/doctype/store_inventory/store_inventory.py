# -*- coding: utf-8 -*-
# Copyright (c) 2021, Mainul Islam and contributors
# For license information, please see license.txt

# from __future__ import unicode_literals
# # import frappe
# from frappe.model.document import Document

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document
from frappe import _
from frappe.utils import getdate, nowdate, cint, flt


class StoreInventory(Document):
	pass


@frappe.whitelist()
def get_item_details(countsheet_type_name):
	item = frappe.db.sql("""
	select items.item_group, items.item_code, items.whole_uom, items.whole_qty, items.broken_uom, items.broken_qty
	from `tabItems` items join `tabCountsheet Template` countsheet_template on items.parent=countsheet_template.name where countsheet_template.countsheet_type_name=%s
	and countsheet_template.docstatus=1 """, countsheet_type_name, as_dict = 1)

	return item if item else None

